package modular.algorithm;

/**
 * 选择排序
 */
public class SelectSort {
    public int[] sort(int[] array){
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            //认为目前的数就是最小的，记录最小数的下标
            int minIndex = i;
            for (int j = i+1; j < array.length; j++) {
                if (array[minIndex] > array[j]){
                    //修改最小值的下标
                    minIndex = j;
                }
            }
            //当退出for就找到这次的最小值，就需要交换位置了
            if (i != minIndex){
                //交换当前值和找到的最小值的位置
                temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;
            }
        }
        return array;
    }

    public static void main(String[] args){
        SelectSort selectSort = new SelectSort();
        int[] array = {2,5,1,6,4,9,8,5,3,1,2,0};
        int[] sort = selectSort.sort(array);
        for (int num : sort) {
            System.out.println(num+"\t");
        }
    }
}
