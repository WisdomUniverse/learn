package modular.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/*这个小程序是重新设置打印输出的窗口，
* 把默认在命令行窗口输出打印内容设置成其他指定的打印显示窗口
*/
public class TestPrintStream {
    public static void main(String[] args) {
        PrintStream ps = null;
        try {
            FileOutputStream fos = new FileOutputStream("D:\\F-FILE-MANAGE\\桌面文件\\log.txt");
            ps = new PrintStream(fos); //在输出流的外面套接一层打印流，用来控制打印 输出
            //这里调用setOut()方法改变了输出窗口，以前写 System.out.print()默认的输出窗口就是命令行窗口.
            //但现在使用System.setOut(ps)将打印输出窗口改成了由ps指定的文件里 面，通过这样设置以后，打印输出时都会在指定的文件内打印输出
            // 在这里将打印输出窗口设置到了log.txt这个文件里面，所以打印出来的内容会 在log.txt这个文件里面看到
            if (ps != null) System.setOut(ps);
            for (char c = 0; c < 1000; c++) {
                System.out.println(c+"\t"); //把世界各国的文字打印到log.txt这个文件 中去
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
