package modular.io;

import java.io.*;

//转换流测试代码
public class TestTransform1 {
    public static void main(String[] args) {
        try {
            // 如果在调用FileOutputStream的构造方法时没有加入true，那么新加入的字符 串就会替换掉原来写入的字符串，在调用构造方法时指定了字符的编码
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("D:\\F-FILE-MANAGE\\桌面文件\\char.txt",true));
            osw.write("我爱你中国"); // 把字符串写入到指定的文件 中去
            System.out.println(osw.getEncoding()); // 使用getEncoding()方法取得 当前系统的默认字符编码
            osw.close(); //关闭转换流
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


class TestTransform2{
    public static void main(String[] args) {
        try {
            //System.in这里的in是一个标准的输入流，用来接收从键盘输入的数据
            //转换流
            InputStreamReader isr = new InputStreamReader(System.in);
            //缓冲流
            BufferedReader br = new BufferedReader(isr);
            String s = br.readLine(); //使用readLine()方法把读取到的一行字符串保存到字符串 变量s中去
            while(s!=null){
                System.out.println(s.toUpperCase()); //把保存在内存s中的字符串打 印出来
                s = br.readLine(); //在循环体内继续接收从键盘的输入
                if (s.equalsIgnoreCase("exit")) break; //只要输入exit循环就结束，就会退出
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
