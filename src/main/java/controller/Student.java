package controller;


public class Student{
    private static int count;
    private int num;
    public Student() {
        count++;
        num++;
    }
    public static void main(String[] args) {
        Student s1 = new Student();
        Student s2 = new Student();
        Student s3 = new Student();
        Student s4 = new Student();
        //因为还是在类中,所以可以直接访问私有属性
        System.out.println(s1.num);
        System.out.println(s2.num);
        System.out.println(s3.num);
        System.out.println(s4.num);
        System.out.println(Student.count);
        System.out.println(s1.count);
        System.out.println(s2.count);
        System.out.println(s3.count);
        System.out.println(s4.count);
    }
}

/*class Person{
    public void print(){
        System.out.println("Person");
    }
}
public class Student extends Person{
    public void print(){
        System.out.println("Student");
    }
    public void test(){
        print(); //Student
        this.print(); //Student
        super.print(); //Person
    }

    public static void main(String[] args) {
        Student student = new Student();
        student.test();
    }
}*/

/*public class Student {
    private String name;

    public Student(){
        System.out.println("this="+this);
    }

    public static void main(String[] args) {
        Student student = new Student();
        System.out.println("student="+student);
    }
}*/

/*public class Student{
    private String name;
    public void test(){
        System.out.println(this);
    }
    public static void main(String[] args) {
        Student s1 = new Student();
        Student s2 = new Student();
        s1.test();
        s2.test();
    }
}*/

/*public class Student extends Person{
    private String name = "lisi";
    public void test(String name){
        System.out.println(name);
        System.out.println(this.name);
        System.out.println(super.name);
    }

    public static void main(String[] args) {
        Student student = new Student();
        student.test("小明");
    }
}

class Person{
    protected String name = "zs";
}*/

