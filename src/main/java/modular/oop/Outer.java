package modular.oop;

/***
 * 成员内部类（实例内部类、非静态内部类）
 * 注：成员内部类中不能写静态属性和方法
 */
public class Outer {
    private int id;
    public void out(){
        System.out.println("这是外部方法");
    }

    //定义一个内部类
    class Inner{
        public void in(){
            System.out.println("这是内部类方法");
        }
    }
}

class Test{
    public static void main(String[] args) {
        //实例化成员类不类
        //1.实例化外部类
        Outer outObject = new Outer();
        //2.通过外部类调用内部类
        Outer.Inner inObject = outObject.new Inner();
        //测试，调用内部类中的方法
        inObject.in();//打印：这是内部类方法
    }
}
