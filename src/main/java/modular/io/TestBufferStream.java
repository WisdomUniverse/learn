package modular.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

//缓冲流
public class TestBufferStream {
    public static void main(String[] args) {
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\F-FILE-MANAGE\\桌面文件\\luo.txt"));
            String s = null;
            for (int i = 0; i < 100; i++) {
                //“Math.random()”将会生成一 系列介于0～1之间的随机数。
                // static String valueOf(double d)这个valueOf()方法的作用就是把 一个double类型的数转换成字符串
                s = String.valueOf(Math.random());
                bw.write(s); //把随机数字符串写入到指定文件中
                bw.newLine(); //调用newLine()方法使得每写入一个随机数就换行显示
            }
            bw.flush(); //调用flush()方法清空缓冲区

            BufferedReader br = new BufferedReader(new FileReader("D:\\F-FILE-MANAGE\\桌面文件\\电脑操作.txt"));
            //在节点流FileReader的外面再套一层处理流BufferedReader
            while ((s = br.readLine()) !=null){
                //使用BufferedReader处理流里面提供String readLine()方法读取文件中 的数据时是一行一行读取的
                // 循环结束的条件就是使用readLine()方法读取数据返回的字符串为空值后则表 示已经读取到文件的末尾了。
                System.out.println(s);
            }
            bw.close();
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
