package modular.algorithm;

import java.util.Calendar;
import java.util.Scanner;

/**
 *  请使用递归算法计算n！
 * Recursion 读法:[rɪˈkɜ:rʃn] 递归
 * 一个正整数的阶乘（factorial）是所有小于及等于该数的正整数的积，并且0的阶乘为1。 自然数n的阶乘写作n!。
 * 1808年，基斯顿·卡曼引进这个表示法。 亦即n!=1×2×3×...×n。阶乘亦可以递归方式定义：0!=1，n!=(n-1)!×n。
 */
public class Recursion {

    //递归算法 阶乘算法
    public int factorial(int num){
        if (num==0 || num==1){
            return 1;
        }else{
            return num*factorial(num-1); //递归调用
        }
    }

    //循环算法
    public long factorialLoop(int num){
        if (num==0 || num==1){
            return 1;
        }else{
            long result = 1;
            for (int i = num; i>0; i--){
                result *=i;
            }
            return result;
        }
    }

    public static void main(String[] args){
        System.out.println("请输入整数：");
        Scanner number = new Scanner(System.in); //等待键盘输入
        int in = number.nextInt();
        System.out.println("输入参数："+in);
        Recursion recursion = new Recursion();

        //递归算法
        long startMillis = Calendar.getInstance().getTimeInMillis();
        int factorial = recursion.factorial(in);
        long endMillis = Calendar.getInstance().getTimeInMillis();
        System.out.println("递归算法耗时="+(endMillis-startMillis));
        System.out.println(factorial);


        //循环算法
        long startMillisFor = Calendar.getInstance().getTimeInMillis();
        long loop = recursion.factorialLoop(in);
        long endMillisFor = Calendar.getInstance().getTimeInMillis();
        System.out.println("递归算法耗时="+(endMillisFor-startMillisFor));
        System.out.println(loop);


    }
}
