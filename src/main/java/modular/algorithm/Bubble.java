package modular.algorithm;

/**
 * 冒泡排序
 */
public class Bubble {

    public int[] sort(int[] array){
        int flagCount = 0;
        int temp;
        //外层循环，决定一共走几趟
        for (int i = 0; i < (array.length - 1); i++) {
            int flag = 0; //通过符号位可以减少无谓的比较，如果已经有序了，就退出循环
            //内存循环，它决定每趟走一次
            for (int j = 0; j < (array.length - i - 1); j++) {
                if (array[j+1]>array[j]){ //倒序排列
                    temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                    flag = 1;
                }

                /*if (array[j+1]<array[j]){ //正序排列
                    temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                    flag = 1;
                }*/
            }
            if (flag == 0){
                flagCount ++;
                break;
            }
        }
        System.out.println("flagCount="+flagCount);
        return array;
    }

    public static void main(String[] args) {
        int[] array = {2,9,1,3,5,6,8,7};
        Bubble bubble = new Bubble();
        int[] sort = bubble.sort(array);
        for (int i : sort) {
            System.out.println(i+"\t");
        }
    }
}
